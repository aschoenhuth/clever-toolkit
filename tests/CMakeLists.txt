# IF(NOT ${STATIC_LINKING} MATCHES "1")
	add_executable (tests 
		tests.cpp 
		CoverageMonitorTest.cpp 
# 		../src/CoverageMonitor.cpp
# 		../src/AlignmentPair.cpp
# 		../src/PackedAlignmentPair.cpp
# 		../src/SingleTrackCoverageMonitor.cpp
		VariationIndexTest.cpp
# 		../src/VariationIndex.cpp
		SequenceUtilsTest.cpp
		ShortDnaSequenceTest.cpp
# 		../src/ShortDnaSequence.cpp
		NfaMatcherTest.cpp
		SplitAlignerTest.cpp
		../src/SplitAligner.cpp
		OverlappingRegionsTest.cpp
# 		../src/OverlappingRegions.cpp
		VariationTest.cpp
# 		../src/Variation.cpp
# 		../src/NamedDnaSequence.cpp
		DistributionsTest.cpp
# 		../src/Distributions.cpp
# 		../src/ReadGroups.cpp
		SumOfBinomialsTest.cpp
# 		../src/SumOfBinomials.cpp
	)
	target_link_libraries(tests CleverLib)
	add_test(NAME all-unittests COMMAND tests)
# ENDIF()
